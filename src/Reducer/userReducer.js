import { createSlice } from "@reduxjs/toolkit";
import {userData}  from '../Data/userData'

const userSlice = createSlice({
    name:"kaushal",
    initialState: userData,
    reducers: {
        addUser: (state, action) => {
            state.push(action.payload)
        },
        updateUsers : (state, action) => {
            // dispatch(updateUser({ id: id, name : uname,email:uemail,number:unumber,birthDate:ubirthDate}))
            const {id, name, email, number, birthDate}  = action.payload; 
            const uu = state.find((user) => user.id == id);
            if(uu){
                uu.name = name;
                uu.email = email;
                uu.number = number;
                uu.birth = birthDate

            }
        },
        deleteUser : (state, action) => {
            const {id}  = action.payload; 
            const uu = state.find((user) => user.id == id);
            if(uu){
                return state.filter((item) => item.id != id)
            }
        }
    }
})

export const {addUser,updateUsers,deleteUser} = userSlice.actions;
export default userSlice.reducer











































































