import logo from './logo.svg';
import './App.css';
import ListData from './Single_Crud/List'; // Single Crud
import CreateUser from './Single_Crud/Create'; // Single Crud create
import UpdateUser from './Single_Crud/Update'; // Single Crud create
import { BrowserRouter, Routes, Route } from 'react-router-dom';


function App() {
  return (
   
    <BrowserRouter>
      <Routes>
        <Route path='/' element={ <ListData />}> </Route>
        <Route path='/list' element={ <ListData />}> </Route>
        <Route path='/create' element={ <CreateUser />}> </Route>
        <Route path='/edit/:id' element={ <UpdateUser />}> </Route>
     </Routes>
    
    </BrowserRouter>


  );
}

export default App;
