import React, { useState } from 'react'
import { addUser } from '../Reducer/userReducer'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'


function CreateUser() {

    const [name, setName] = useState('')
    const [email , setEmail] = useState("")
    const [number, setNumber] = useState('')
    const [birthDate, setBirthdate] = useState('')
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const users = useSelector((state)=> state.users)
    const handleSubmit = (event)=> {
        event.preventDefault();
        dispatch(addUser({ id: users[users.length - 1].id + 1, name :name,email:email,number:number,birth:birthDate}))
        navigate('/')
    }

  return (
    <div>
        <h2>Create New User</h2>
        <form onSubmit={handleSubmit}>
                <div>
                    <label htmlFor='name'>Name:</label>
                    <input type='text' onChange={(e)=> setName(e.target.value)} placeholder='Enter name' className='form-control'></input>
                </div>
                <div>
                    <label htmlFor='email'>Email:</label>
                    <input type='email'  onChange={(e)=> setEmail(e.target.value)}  placeholder='Enter email' className='form-control'></input>
                </div>
                <div>
                    <label htmlFor='number'>number:</label>
                    <input type='number' onChange={(e)=> setNumber(e.target.value)}  placeholder='Enter number' className='form-control'></input>
                </div>
                <div>
                    <label htmlFor='date'>birthdate:</label>
                    <input type='date' onChange={(e)=> setBirthdate(e.target.value)}  placeholder='Enter birthdate' className='form-control'></input>
                </div>
                <br/>
                <button type='submit'>
                    Submit
                </button>
        </form>
    </div>
  )
}

export default CreateUser