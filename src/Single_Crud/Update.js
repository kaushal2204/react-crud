import React, { useState } from 'react'
import { updateUsers } from '../Reducer/userReducer'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'


function UpdateUser() {

    const {id} = useParams()
    const users = useSelector((state)=> state.users)
    const existingUser = users.filter(f => f.id == id)
    const {name, email, number, birth} = existingUser[0]
    const [uname, setName] = useState(name)
    const [uemail , setEmail] = useState(email)
    const [unumber, setNumber] = useState(number)
    const [ubirthDate, setBirthdate] = useState(birth)
    const navigate = useNavigate()
    const dispatch = useDispatch()

    const handleUpdate = (event)=> {
        event.preventDefault();
        dispatch(updateUsers({ id: id, name : uname,email:uemail,number:unumber,birthDate:ubirthDate}))
        navigate('/')
    }

  return (
    <div>
        <h2>Update User</h2>
        <form onSubmit={handleUpdate}>
                <div>
                    <label htmlFor='name'>Name:</label>
                    <input type='text' value={uname} onChange={(e)=> setName(e.target.value)} placeholder='Enter name' className='form-control'></input>
                </div>
                <div>
                    <label htmlFor='email'>Email:</label>
                    <input type='email' value={uemail}  onChange={(e)=> setEmail(e.target.value)}  placeholder='Enter email' className='form-control'></input>
                </div>
                <div>
                    <label htmlFor='number'>number:</label>
                    <input type='number'  value={unumber} onChange={(e)=> setNumber(e.target.value)}  placeholder='Enter number' className='form-control'></input>
                </div>
                <div>
                    <label htmlFor='date'>birthdate:</label>
                    <input type='date'  value={ubirthDate} onChange={(e)=> setBirthdate(e.target.value)}  placeholder='Enter birthdate' className='form-control'></input>
                </div>
                <br/>
                <button type='submit'>
                    Submit
                </button>
        </form>
    </div>
  )
}

export default UpdateUser