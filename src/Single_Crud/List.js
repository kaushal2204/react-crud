import React,{useEffect, useState} from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { deleteUser } from '../Reducer/userReducer'

function ListData() {
  const users = useSelector((state)=> state.users)
  const dispatch = useDispatch()

  useEffect(() => {
    console.log(users)
  }, [])
  
  const handleDelete = (id) => {
    dispatch(deleteUser({id:id}))
  }
  return (
    // <div>list</div>
    <div className='container'>
      <h2>Crud App with json server</h2>
      <Link to='/create'>
      Add User
      </Link>
      <table className='table'>
        <thead>
          <tr>
            <th>No</th>
            <th>Name</th>
            <th>Email</th>
            <th>Number</th>
            <th>Action</th>

          </tr>
        </thead>
    <tbody>
          {users && users.map((item,index)=>{
           return <tr key={index}>
              <th>{index + 1}</th>
              <th>{item.name}</th>
              <th>{item.email}</th>
              <th>{item.number}</th>
              <th>
                <div className='m-2'>
                  <Link to={`/edit/${item.id}`}>
                    Edit
                  </Link>
                  <button onClick={()=> handleDelete(item.id)}>
                    Delete
                  </button>
                </div>
              </th>
            </tr>
          })}
    </tbody>
      </table>

    </div>
  )
}

export default ListData